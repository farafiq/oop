<?php

//release 0
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$sheep = new Animal("shaun");

echo "Nama Hewan : $sheep->name <br>"; // "shaun"
echo "Jumlah Kaki : $sheep->legs <br>"; // 2
echo "Berdarah Dingin : $sheep->cold_blooded <br>"; // false

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

print "<br>";


//release 1

$sungokong = new Ape("kera sakti");
echo "Nama Hewan : $sungokong->name <br>"; 
echo "Jumlah Kaki : $sungokong->legs <br>";
echo "Suara : ";
$sungokong->yell(); // "Auooo"

print "<br>";
print "<br>";


$kodok = new Frog("buduk");
echo "Nama Hewan : $kodok->name <br>"; 
echo "Jumlah Kaki : $kodok->legs <br>";
echo "Suara : ";
$kodok->jump() ; // "hop hop"
?>